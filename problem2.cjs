function problem2(inventory){
    if(inventory==undefined || inventory==[] ){
        return [];
    }
    if(Array.isArray(inventory)==false){
        return [];
    }
    let last=inventory.length-1;
    return inventory[last];
}
module.exports=problem2;