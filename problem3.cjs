function problem3(inventory){
    if(inventory==undefined || inventory==[]){
        return [];
    }
    if(Array.isArray(inventory)==false){
        return [];
    }
    let n=inventory.length;
    inventory.sort((elementOne,elementTwo)=>{
        return elementOne["car_model"]>elementTwo["car_model"];
    });
    let x=inventory.map((element)=>(element["car_model"]));
    return x;
}
module.exports=problem3;