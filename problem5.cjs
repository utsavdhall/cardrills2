const problem4 =require("./problem4.cjs");
function problem5(inventory){
    if(inventory==undefined || inventory==[]){
        return [];
    }
    if(Array.isArray(inventory)==false){
        return [];
    }
    let years=problem4(inventory);
    let answers=years.filter((year)=>{
        return year<2000;
    });
    return answers;
}

module.exports=problem5;