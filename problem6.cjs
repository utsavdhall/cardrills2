
function problem6(inventory){
    if(inventory==undefined || inventory==[]){
        return [];
    }
    if(Array.isArray(inventory)==false){
        return [];
    }
    let BMWAndAudi=inventory.filter((item)=>{
        return item["car_make"]=="Audi" || item["car_make"]=="BMW";
    });
    return JSON.stringify(BMWAndAudi);
}
module.exports=problem6;