function problem4(inventory){
    if(inventory==undefined || inventory==[]){
        return [];
    }
    if(Array.isArray(inventory)==false){
        return [];
    }
    let years=inventory.map((item)=>item["car_year"]);
    return years;
}
module.exports=problem4;