function problem1(inventory,input){
    let ans=[];
    if(inventory==undefined || input==undefined){
        return ans;
    }
    if(input.length==0 || inventory.length==0){
        return ans;
    }
    if(Array.isArray(inventory)==false){
        return ans;
    }
    inventory=inventory.filter((inven) => inven.id==input);
    return inventory;
}

module.exports=problem1;
